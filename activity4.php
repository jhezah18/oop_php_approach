<center>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<br><br>
<form validateMe="number4.php" method="post">
             Name: <input type="text" name="username" required><br><br>
             Address <input type="text" name="address" required><br><br>
             Age: <input type="number" name="age" required> <br><br>
             Mobile Number: <input type="text" name="mobilenumber" required> <br>
             <br>
             <input  type="submit">
 </form>
</body>
</html>
<?php
class Validate
{

    public function __construct()
    {
        $this->prob1 = isset($_POST['username']) ? $_POST['username'] : null;
        $this->prob2 = isset($_POST['address']) ? $_POST['address'] : null;
        $this->prob3 = isset($_POST['mobilenumber']) ? $_POST['mobilenumber'] : null;
        $this->prob4 = isset($_POST['age']) ? $_POST['age'] : null;
    }

    public function validateHere()
    {
        if (!preg_match("/^[a-zA-z]*$/", $this->prob1)) {
            $ErrMsg = "Only alphabets and whitespace are allowed.<br>";
            echo $ErrMsg;
        } else if($this->prob1=="") {
            echo $this->prob1 . "- Please Enter a valid Name<br>";
        }else{
            echo $this->prob1 . " - Name is valid <br>";
        }

        if(!preg_match('/^(?:\\d+ [a-zA-Z ]+, ){2}[a-zA-Z ]+$/', $this->prob2)){
            echo $this->prob2 ."- Invalid Address<br>";
        }else{
            echo $this->prob2 ." - Valid Address<br>";
        }
      

        if ($this->prob4 >= 18) {
            echo $this->prob4 ." - is a legal age<br>";
        } else {
            echo $this->prob4 ." - Age Must be 18 above<br>";
        }

        $length = strlen($this->prob3);

        if ($length < 11 || $length > 11    ) {
            $ErrMsg = $this->prob3 ."- Must be 11 digits";
            echo $ErrMsg;
        } else {
            echo $this->prob3 ."- Phone number is valid";
        }
        
    }
}


$validate = new Validate();

$validate->validateHere();
?>
</center>
