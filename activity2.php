<center>
<?php
 
 session_start();

 class userInput{
 
    public $user_Input;

    function __construct($user_Input){

         $this->user_Input = $user_Input;

    }

    function addInput(){

        if(!isset($_SESSION['Option'])){
            $_SESSION['Option'] = [];
        }

        array_push($_SESSION['Option'],$this->user_Input);
        
        echo '<select name="dropdown">';
                
        foreach ($_SESSION['Option'] as $value) {
            echo '<Option value="'.$value.'">"'.$value.'"</Option>' ;
        }
        
        echo '</select>';
              
    }

 }

if(isset($_POST['submit'])){
    $user_Input = $_POST['user_Input'];
    $input = new userInput($user_Input);
    $input->addInput();
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="" method="post">
       Enter Text :
        <input type="text" name="user_Input">
        <br>
        <br>
        <button type="submit" name="submit">Submit</button>
    </form>
</body>
</html>
</center>
